const getdata = document.querySelector(".data");
const url = "http://localhost:8000/users";
const addUserForm = document.querySelector(".add-user-form");
const usernamevalue = document.getElementById("username-value");
const emailvalue = document.getElementById("email-value");
const passwordvalue = document.getElementById("password-value");
const rolevalue = document.getElementById("role-value");
const buttonSubmit = document.querySelector(".btn")


let temp = "";
const renderUsers = (data) => { 
  console.log(data); 
  data.forEach((itemData) => {
    temp += `<tr class="line-body" data-id=${itemData.id}>`;
    temp += "<td>" + itemData.id + "</td>";
    temp += "<td class='line-username'>" + itemData.username + "</td>";
    temp += "<td class='line-email'>" + itemData.email + "</td>";
    temp += "<td class='line-password'>" + itemData.password + "</td>";
    temp += "<td class='line-role'>" + itemData.role + "</td>";
    temp += '<td> <a href="#" class="link-primary" id="edit_user">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;';
    temp +=
      ` <a href="#" class="link-danger" id="delete_user">Delete</a></td></tr>`;
  });
  getdata.innerHTML = temp;
};

// GET /users
fetch(url)
  .then((res) => res.json())
  .then((data) => renderUsers(data));

// Create / insert new user
// Method : POST
addUserForm.addEventListener('submit', (e) => {
  e.preventDefault();
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: usernamevalue.value,
      email: emailvalue.value,
      password: passwordvalue.value,
      role: rolevalue.value
    })
  })
    .then(res => res.json())
    .then(data => {
      if(data.status==200){
        renderUsers([data.user]);
      }
      
    })

    //reset input field to empty 
    usernamevalue.value = '';
    emailvalue.value = '';
    passwordvalue.value = '';
    rolevalue.value = '';
})



//edit user
getdata.addEventListener('click', (e) => {
    e.preventDefault();
    let delButtonIsPressed = e.target.id == 'delete_user';
    let editButtonIsPressed = e.target.id == 'edit_user';

    let id = e.target.parentElement.parentElement.dataset.id;
    
    // Delete - Remove the existing post
    // method: DELETE
    if(delButtonIsPressed) {
        fetch(`${url}/${id}`, {
          method: 'DELETE',
        })
        .then(res => res.json())
        .then(() => location.reload())
    }

    //edit - update the existing user
    //method - PUT 
    if(editButtonIsPressed){
      const parent = e.target.parentElement.parentElement;
      let usernameContent = parent.querySelector('.line-username').textContent;
      let emailContent = parent.querySelector('.line-email').textContent;
      let passwordContent = parent.querySelector('.line-password').textContent;
      let roleContent = parent.querySelector('.line-role').textContent;

      usernamevalue.value = usernameContent;
      emailvalue.value = emailContent;
      passwordvalue.value = passwordContent;
      rolevalue.value = roleContent;

      buttonSubmit.addEventListener('click', (e) => {
        e.preventDefault()
        fetch(`${url}/${id}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: usernamevalue.value,
            email: emailvalue.value,
            password: passwordvalue.value,
            role: rolevalue.value
          })
        })
        .then(res => res.json())
        .then(() => location.reload())
      })
      

    }

});

