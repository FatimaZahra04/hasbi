var { User } = require("../models/");

// exportation
module.exports = {
  getAllUsers: function () {
    return User.findAll();
  },
  getUsers: function (offset = 0, limit = 10) {
    return User.findAll({ offset: offset, limit: limit });
  },
  getUserByRole: function (role) {
    return User.findAll({
      where: {
        role: role,
      },
    });
  },
  getAdmins: function () {
    return this.getUserByRole("admin");
  },
  getAuthors: function () {
    return this.getUserByRole("author");
  },
  getGuests: function () {
    return this.getUserByRole("guest");
  },

  getUser: function (id) {
    return User.findOne({
      where: { id: id },
    });
  },

  getUserByEmail: function (email) {
    return User.findOne({
      where: {
        email: email,
      },
    });
  },

  addUser: async function (userInfo) {
    user = await this.getUserByEmail(userInfo.email);
    if (user)
      return {
        status: 403,
        message: "User already exists",
      }; 
    var newuser = await User.create(userInfo);
    return {
      status: 200,
      message: "User added succesuflly",
      user: newuser,
    };
  },

  updateUser: async function (id, userInfo) {
    return await User.update(userInfo, {
      where: {
        id: id,
      },
    });
  },
  deleteUser: async function (id) {
    return await User.destroy({
      where: {
        id: id,
      },
    });
  },
};
