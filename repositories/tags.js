const { Tag } = require('../models')
 module.exports = {
   getAllTags() {
     return Tag.findAll()
   },
   getTags(offset = 0, limit = 10){
    return  sequelize.query("SELECT * FROM Tags LIMIT "+ limit +" OFFSET "+offset)
  } ,


   getTags(id) {
    var x= Tag.findAll({
      where: {       
          id: id
      }
    });
    return x
    },

 
    async addTags(tag) { 
      return await Tag.create(tag);
    },
      

    async updateTags(id,tag) { 
    return await Tag.update(tag, {
      where: {
        id: id
      }
    });
    },


    async deleteTags(id) { 
    await Tag.destroy({
      where: {
        id: id
      }
    });
  },
 }