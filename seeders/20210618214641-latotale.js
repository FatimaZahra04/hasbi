'use strict';
var faker = require('faker');
// const { between } = require("sequelize/types/lib/operators.d.ts");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    //Users
    const users = [...Array(20)].map((user) => (
      {
          email: faker.internet.email(),
          userName: faker.internet.userName(),
          password: faker.internet.password(12),
          role: faker.random.arrayElement(['admin','author','guest']),
          createdAt: faker.date.between('2000-01-01', '2021-01-01'),
          updatedAt: faker.date.between('2021-01-01', '2021-06-1')
      }
    ))
    await queryInterface.bulkInsert('Users', users, {});

    //Tags

    var tags = [...Array(10)].map((tag) => (
      {
         
          name: faker.name.findName()+ faker.name.findName() + faker.name.findName(),
          createdAt: faker.date.between('2000-01-01', '2021-06-01'),
          updatedAt: faker.date.between('2021-01-01', '2021-06-1')
      }
    ))
    await queryInterface.bulkInsert('Tags', tags, {});

    //Articles
    var articles = [...Array(10)].map((article) => (
      {
          title: faker.name.title(),
          content: faker.lorem.sentences(),
          published:faker.random.boolean(),
          createdAt: faker.date.between('2021-01-01', '2021-06-01'),
          updatedAt: faker.date.between('2021-01-01', '2021-06-1'),
          UserId:faker.random.arrayElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20])
      }
    ))
    await queryInterface.bulkInsert('Articles', articles, {});

    //Article tag

    var articleTag = [...Array(10)].map((articleTag) => (
      {
          createdAt: faker.date.between('2000-01-01', '2021-06-01'),
          updatedAt: faker.date.between('2021-01-01', '2021-06-1'),
          ArticleId: faker.random.arrayElement([1,2,3,4,5,6,7,8,9,10]),
          TagId: faker.random.arrayElement([1,2,3,4,5,6,7,8,9,10])
      }
    ))
    
    await queryInterface.bulkInsert('Articletags', articleTag, {});


    //Comments
    var comments = [...Array(10)].map((comments) => (
      {
          content: faker.lorem.sentences(),
          createdAt: faker.date.between('2000-01-01', '2021-06-01'),
          updatedAt: faker.date.between('2021-01-01', '2021-06-1'),
          ArticleId: faker.random.arrayElement([1,2,3,4,5,6,7,8,9,10]),
          
      }
    ))
    await queryInterface.bulkInsert('Comments', comments, {});

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
    await queryInterface.bulkDelete('Tags', null, {});
    await queryInterface.bulkDelete('Articles', null, {});
    await queryInterface.bulkDelete('Articletags', null, {});
    await queryInterface.bulkDelete('Comments', null, {});
  }
};
