const express = require('express');
const cors = require('cors');
const app = express();

const Router 			= require('./routes/router')
const authMiddleware 	= require('./middlewares/auth')
app.use(authMiddleware, Router.private);
app.use(Router.public);


app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());


app.get('/', (req, res)=>{
    res.send('Hello World');
});

//listen to the port
app.listen(8000, function(){
    console.log(`App is running at port 8000`);
});




// const express = require('express');


// const cors = require('cors');
// const app = express();

// app.use(express.json());
// app.use(express.urlencoded({extended:true}))
// app.use(cors());

// const usersRouter = require('./routes/users');
// const articlesRouter = require('./routes/articles');
// const commentsRouter = require('./routes/comments');
// const tagsRouter = require('./routes/tags');

// app.use('/users', usersRouter);
// app.use('/articles', articlesRouter);
// app.use('/comments', commentsRouter);
// app.use('/tags', tagsRouter);


// app.get('/', (req, res)=>{
//     res.send('Hello World');
// });

// // listen to the port
// app.listen(8000, function(){
//     console.log(`App is running at port 8000`);
// });

