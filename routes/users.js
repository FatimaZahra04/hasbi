const usersRepo = require("../repositories/users");

module.exports = function(router){
/* GET /users  */
router.get("/", async function (req, res) {
    // if(req.user.role != 'admin')
	// 			return res.status(403).send({
	// 				message: 'Access denied'
	// 			}); 
    res.send(await usersRepo.getAllUsers());
});

/* Get user by Id */
router.get('/:id', async function (req, res) {
    const user = await usersRepo.getUser(req.params.id);
    if (user === null)
      res.status(404).send({ status: 'Not Found' })
    else{
        res.send(user);
    }
  } )
 
/* POST /users (add user) */
router.post('/', async function (req, res){
    const user = await usersRepo.addUser(req.body);
    res.send(user)
});

/* PUT Modifier un utilisateur */
 router.put('/:id', (req, res) => {
    const user = usersRepo.updateUser(req.params.id, req.body)
    res.send({status: "OK User Updated", user})
  })

/*  DELETE Supprimer un utilisateur */
 router.delete('/:id', (req, res) => {
    const user = usersRepo.deleteUser(req.params.id)
    res.send({status: "OK User removed", user})
  })
}


// const usersRepo = require("../repositories/users");

// module.exports = function (router) {
//   /* GET /users  */
//   router.get("/", async function (req, res) {
//     // if(req.user.role != 'admin')
//     // 			return res.status(403).send({
//     // 				message: 'Access denied'
//     // 			});
//     res.send(await usersRepo.getAllUsers());
//   });

//   /* Get user by Id */
//   router.get("/:id", async function (req, res) {
//     const user = await usersRepo.getUser(req.params.id);
//     if (user === null) res.status(404).send({ status: "Not Found" });
//     else {
//       res.send(user);
//     }
//   });

//   /* POST /users (add user) */
//   router.post("/", async function (req, res) {
//     const user = await usersRepo.addUser(req.body);
//     res.send(user);
//   });

//   /* PUT Modifier un utilisateur */
//   router.put("/:id", (req, res) => {
//     const user = usersRepo.updateUser(req.params.id, req.body);
//     res.send({ status: "OK User Updated", user });
//   });

//   /*  DELETE Supprimer un utilisateur */
//   router.delete("/:id", (req, res) => {
//     const user = usersRepo.deleteUser(req.params.id);
//     res.send({ status: "OK User removed", user });
//   });
// };