var privateRouter 	= require("express").Router()
var publicRouter 	= require("express").Router()

// auth router
require('./auth')(publicRouter)

require('./users')(privateRouter)
require('./articles')(privateRouter)
require('./comments')(privateRouter)
require('./tags')(privateRouter)

module.exports ={
	private: privateRouter,
	public: publicRouter
}	
